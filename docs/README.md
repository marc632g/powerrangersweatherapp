---
home: true
heroImage: /images/Weather.svg
heroText: Power Rangers Weather
tagline: Amazing weather app!
actionText: Contribute →
actionLink: /contribute/
features:
    - title: Simplicity First
      details: Minimal information, who needs more anyways
    - title: Easy to read
      details: Well, it's easy to read
    - title: Performant
      details: Do not even blink :O
footer: Power Rangers Weather Org.
---
