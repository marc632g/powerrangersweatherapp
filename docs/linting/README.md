# Linting

We use eslint to enforce our coding standard.

Project's Eslint Setup:
<img :src="$withBase('/images/Eslint.PNG')" alt="Eslint config image">

### We use couple of standard rules for eslint:

-   Rules for semicolon: "semi": ["error"],
    It adds error every time you have an extra semicolon, this is to reduce unnecessary code for the project.

-   Rules for double quotes: [“error”,”double”]
    By this we specify that we want to use double instead of single quotes, this is due to our
    vim file which is configured mostly with double quotes.

-   Rules for undefined variables: [“off”]
    It turns off the rule for undefined variables.
