# Continuous Delivery

The project is set up to make use of a GitLab CI / CD.

By putting a congifuration file(.gitlab-ci.yml) into a root of our project we can tell GitLab to automatically perform requested actions.

In our case we configured GitLab to:

-   Build the application,
-   Run all tests,
-   Deploy the application to GitLab pages

:::tip
To run the pipeline all you have to do is to push new changes to master branch of the repository.
:::

##### GitLab configuration

```yml
image: node:latest

cache:
    paths:
        - node_modules/

build:
    stage: build
    script:
        - npm install
        - npm run build

test_async:
    stage: test
    script:
        - npm install
        - npm test

pages:
    stage: deploy
    script:
        - echo "do nothing"
    artifacts:
        paths:
            - public
    only:
        - master
```
