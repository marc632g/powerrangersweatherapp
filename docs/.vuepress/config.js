module.exports = {
  title: "Dev Env mandatory 1",
  description: "Documentation",
  base: "/powerrangersweatherapp/",
  dest: "public",
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "Contribute", link: "/contribute/" },
      { text: "CI/CD", link: "/integration/" },
      { text: "Testing", link: "/testing/" },
      { text: "Linting", link: "/linting/" },
      { text: "VIM", link: "/vim/" },
      // { text: "Project", link: "/project/" },
    ],
    // sidebar: {
    //     "/project/": ["", "frontend", "backend"],
    // },
  },
};
