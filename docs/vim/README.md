# VIM setup
When setting up our VIM, we wanted to implement convenience, we were used to, known from more mainstream IDEs (VS Code, Atom, JetBrains IDEs etc.). In addition to that, we had to add tools, which would help us with software quality assurance (e.g. linting, continuous integration) 

## Basic settings
Our vimrc file consists of basic configuration settings most users would find helpful when utilising Vim. We decided to add settings which make writing in Vim a more pleasurable experience.

## Plugins

### Plugin manager
To install all of the plugins we have used [vim-plug](https://github.com/junegunn/vim-plug).

#### Unix install command

```sh
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

#### Windows install command
```powershell
iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
    ni $HOME/vimfiles/autoload/plug.vim -Force
```

#### Installing plugins from `.vimrc` file
After installing vim-plug on our machine and having saved .vimrc file in our home directory, we could install all of the listed plugins. In order to do so, we have to open the VIM editor and type in `:PlugInstall` command and press enter.


### Plugins used:
#### Emmet
`Plug 'mattn/emmet-vim'`
It is a plugin, which provided us with familiar support for expanding abbreviations, similar to the ones found in the full version of [Emmet](https://www.emmet.io)

#### Vim-polyglot
`Plug ‘sheerun/vim-polyglot’`
A collection of language packs for Vim. Polyglot bundles several other syntax plugins for over 100 languages, and loads them on demand so the performance is not affected. 

#### Conquer of Completion
`Plug ‘neoclide/coc.nvim’`
It is built upon the concept of language servers, which power features like auto-completion, go-to-definition, hover tooltips, and more in modern editors.

#### Gruvbox
`Plug ‘gruvbox-community/gruvbox’`
Gruvbox is designed as a bright theme with retro colors and light/dark mode. The main focus when developing gruvbox is to keep colors easily distinguishable, create a good contrast and pleasant for the eyes workspace. 
