# Testing

## Jest

To perform unit tests, we use a Node-based testing library for JavaScript called Jest.

[Read more in the Jest documentation.](https://jestjs.io/docs/en/getting-started)

We used [Parcel](https://parceljs.org/) as a bundler because it requires zero configuration (compared to Babel or Webpack) and our weather app is really simple. The bundler is primarily required to run Jest.

## Running tests

To run existing tests in the weather app project, simply run in the project root folder:

```
npm test
```

Jest will look for test files with any of the following popular naming conventions:

- Files with .js suffix in `__tests__` folders.
- Files with `.test.js` suffix.
- Files with `.spec.js` suffix.

## Writing tests

First make sure the code you want to test is exported from the file with `module.exports = <functionName>`. Then import it into your test file that has the above mentioned suffixes or is in a specific test folder.

The most common way to write a test is to add `it()` (or `test()`) blocks with the name of the test and its code. You can also use `describe()` blocks for logical grouping.

Jest provides a built-in `expect()` global function for making assertions. It uses "matchers" to test values in different ways.

A few commonly used matchers:

- `.toBe()`
- `.toEqual()`
- `.toBeNull()`
- `.toBeDefined()`
- `.not.toBeUndefined()`

[See the full list of matchers.](https://jestjs.io/docs/en/expect.html)

## Test example

The function you want to test:

```js
function sum(a, b) {
  return a + b;
}
module.exports = sum;
```

In your test file called `sum.test.js`:

```js
const sum = require("./sum");

test("adds 1 + 2 to equal 3", () => {
  expect(sum(1, 2)).toBe(3);
});
```

To check if your test passed or failed, just run:

```
npm test
```
