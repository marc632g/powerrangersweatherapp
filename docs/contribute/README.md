# Contribute

Here you can read how to contribute to the project and the documentation.

## Contributing to the project

To contribute to the project you have to clone the repository:

[https://gitlab.com/denisekea/weather-testing](https://gitlab.com/denisekea/weather-testing)

After you cloned the repository make a new branch, navigate to the project's root directory and run:

```
npm install
```

To run the project in the local environment run:

```
npm start
```

To run the tests run:

```
npm test
```

### Publishing the changes

After you are done with your changes, rebase your branch into master and create a pull request.
The pull request will be reviewed by other developers and accepted/rejected.

## Contributing to the documentation

To contribute to the documentation you have to clone the documentation's repository:

[https://gitlab.com/marc632g/powerrangersweatherapp](https://gitlab.com/marc632g/powerrangersweatherapp)

After you cloned the repository create a new branch, navigate to the project's root directory and run:

```
npm install
```

To run the documentation in the local environment run:

```
npm run docs:dev
```

:::tip
Sometimes the local environment does not reflect the changes in the code automatically.
In such case please restart the service.
:::

### Creating new page

To add a new page, create a new folder in a docs root directory and place a README.md file inside the created folder.

:::tip
To add the page to a navigation bar go to the config.js file in a docs/.vuepress directory and add a new entry in the 'nav' array inside a themeConfig object.

```js
   themeConfig: {
        nav: [
            { text: "Home", link: "/" },
            { text: "Contribute", link: "/contribute/" },
            { text: "<NewPageName>", link:"/<NewPagePath>/" }
        ],
        ...
    },
```

:::

For further configuration please follow official guide available at:

[https://vuepress.vuejs.org/guide/](https://vuepress.vuejs.org/guide/)

### Publishing the changes

After you are done with your changes, rebase your branch into master and create a pull request.
The pull request will be reviewed by other developers and accepted/rejected.
